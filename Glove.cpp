class Gloves {
public:
    int findMinimum(int n, vector<int> left, vector<int> right)
    {
        int sum_zero=0;
        int left_sum=0,left_min=9999;
        int right_sum=0,right_min=9999;
        for(int i=0;i<n;i++)
        {
            if( left[i]==0 || right[i]==0 )
            {
                sum_zero+=left[i]+right[i];
            }
            else
            {
                left_sum+=left[i];
                right_sum+=right[i];

                left_min=min(left_min,left[i]);
                right_min=min(right_min,right[i]);
            }
        }
        int _min=min(left_sum-left_min+1,right_sum-right_min+1);
        return sum_zero+_min+1;
    }
};